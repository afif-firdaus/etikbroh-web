/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/jsx-no-comment-textnodes */
import Head from 'next/head'
import { useState } from 'react'
import { Container, Col } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import router from 'next/router'


export default function Home() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [openPass, setOpenPass] = useState(true)
  
  const handleSubmit = (e) => {
    e.preventDefault()
    if (password === 'admin321' && username === 'admin') {
      window.localStorage.setItem('login', true)
      router.push('/dashboard')
    } else {
      alert('username atau password salah!')
    }
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Login - Tikbroh Admin</title>
        <meta name="description" content="login page tikbroh web" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <main className={styles.main}>
        <Container>
          <Col lg={4} className="mx-auto mt-5">
            <form style={{border: '1px solid #eaeaea', padding: '0 50px 50px 50px', borderRadius: '16px', boxShadow: '0px 6px 24px #E9E9E9'}}>
              <img className="mb-0 mx-auto w-auto d-flex" src="/icon.png" alt="logo tikbroh" width="172" height="172"/>
              <h1 className="h3 mb-4 fw-bold mx-auto d-flex text-center">Login</h1>
      
              <div style={{display: 'flex', flexDirection: 'column', marginBottom: '15px'}}>
                <label htmlFor="">Username</label>
                <input
                  type="text"
                  placeholder="username"
                  className="input"
                  style={{borderRadius: '4px', border: '1px solid #eaeaea', padding: '10px'}}
                  onChange={(e) => setUsername(e.target.value)}
                  value={username}
                />
              </div>
              <div style={{display: 'flex', flexDirection: 'column', marginBottom: '25px'}}>
                <label htmlFor="">Password</label>
                <div style={{position: 'relative'}}>
                  <input
                    type={openPass ? 'password' : 'text'}
                    placeholder="Password"
                    className="input w-100"
                    style={{borderRadius: '4px', border: '1px solid #eaeaea', padding: '10px', position: 'relative'}}
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                  />
                  <img 
                    src="/eye.svg"
                    width="20"
                    style={{position: 'absolute', right: 10, top: '16px', cursor: 'pointer'}}
                    onClick={() => setOpenPass(!openPass)}
                  />
                </div>
              </div>
              <button 
                className="w-100 btn btn-lg btn-primary"
                style={{fontSize: '16px', fontWeight: 'bold'}}
                onClick={handleSubmit}
              >
                Sign in
              </button>
            </form>
          </Col>
        </Container>
      </main>
    </div>
  )
}

