/* eslint-disable @next/next/no-img-element */
import Head from 'next/head'
import { useState, useEffect } from 'react'
import { Container, Col, Table } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import firebase from '../config/firebase'
import moment from 'moment'
import router from 'next/router'


export default function Home() {
  const [isLoggin, setIsLoggin] = useState(typeof window !== "undefined"  && localStorage.getItem('login'))
  const [datas, setDatas] = useState();
  const [dataValue, setDataValue] = useState();

  useEffect(() => {
    if (!isLoggin) {
      router.push('/')
    } else {
      // getDataFirebase()
      // const urlData = firebase.database().ref(`alamat`);
      // urlData.on('value', function(snapshot) {
      //   const data = [];
      //   const noteData = snapshot.val();
      //   // console.log(data,  'databaru');
      //   if (noteData !== null) {
      //     Object.keys(snapshot.val()).map(key => {
      //       // console.log(key, 'ini key');
      //       data.push(
      //         // key,
      //         snapshot.val()[key]
      //       )
      //     });
      //   }

      firebase.database().ref(`alamat/`).on('value', function(snapshot) {
        const data = [];
        snapshot.forEach(function(childSnapshot) {
          const childData = childSnapshot.val();
          if (childData !== null) {
            Object.keys(childSnapshot.val()).map(key => {
              // console.log(key, 'ini key');
              data.push(
                // key,
                childSnapshot.val()[key]
              )
            });
          }
          // console.log(data, 'dadada');
        });
        setDatas(data)
      })
  
        }
      }, [isLoggin])
      

  const logout = () => {
    window.localStorage.removeItem('login')
    router.push('/')
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Dashboard - Tikbroh Admin</title>
        <meta name="description" content="login page tikbroh web" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <Container>
          <Col>
            <div className="d-flex justify-content-between w-100 align-items-center">
              <div className="d-flex flex-row align-items-center">
                <img src="/icon.png" alt="logo tikbroh" width="90" height="90"/>
                <h1 style={{fontSize: '30px', fontWeight: 'bold', marginBottom: 0}}>Tikbroh Admin Page</h1>
              </div>
              <button 
                className="btn btn-lg btn-primary"
                style={{fontSize: '16px', fontWeight: 'bold', height: 'max-content'}}
                onClick={logout}
              >
                Logout
              </button>
            </div>
            <Table striped bordered hover justify="true" responsive="lg" className="mt-4">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nama</th>
                  <th>No HP</th>
                  <th>Berat Sampah</th>
                  <th>Alamat</th>
                  <th>Catatan</th>
                  <th>Status</th>
                  {/* <th>Action</th> */}
                </tr>
              </thead>
              <tbody>
              {
                datas !== undefined ? datas.map((data, index) => {
                  if (data.status === 'Sedang dijemput' || data.status === 'Pending') {
                  return (
                    <tr key={index}>
                      <td>{moment(data.date).format('l')}</td>
                      <td>{data.name}</td>
                      <td>{data.phone}</td>
                      <td>{data.jumlah} kg</td>
                      <td>{data.alamat}</td>
                      <td>{data.catatan}</td>
                      <td>{data.status}</td>
                      {/* <td>
                        <button className="btn-primary btn">Selesai</button>
                        <button className="btn-danger btn mt-md-2 mt-lg-0 ml-md-0 ml-lg-2">Batalkan</button>
                      </td> */}
                    </tr>
                  )
                  }
                }): <tr style={{textAlign: 'center', marginTop: 20, color: 'gray'}}>Data kosong</tr>
              }
              </tbody>
            </Table>
          </Col>
        </Container>
      </main>
    </div>
  )
}

