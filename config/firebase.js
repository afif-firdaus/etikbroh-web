// Import the functions you need from the SDKs you need
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
// import 'firebase/database';
// import * as firebase from 'firebase'
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  // measurementId: "G-SWK7FTCVC8"
  apiKey: "AIzaSyAo-1-pDvwxlPPplo-seH3U--U79Y8OHcA",
  authDomain: "tikbroh.firebaseapp.com",
  databaseURL: "https://tikbroh-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "tikbroh",
  storageBucket: "tikbroh.appspot.com",
  messagingSenderId: "730524991413",
  appId: "1:730524991413:web:c6bb6bf736312ecdaca3fc",
};

// Initialize Firebase
if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}
// const analytics = getAnalytics(app);

// let app;
// if(firebase.apps.length === 0) {
//   app = firebase.initializeApp(firebaseConfig);
// } else {
//   app = firebase.app();
// }

// export const database = firebase.database();

// export {auth};
export default firebase;